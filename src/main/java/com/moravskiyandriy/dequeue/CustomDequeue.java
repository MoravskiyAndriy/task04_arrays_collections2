package com.moravskiyandriy.dequeue;

import java.util.*;

public class CustomDequeue<T> implements Deque<T> {
    private List<T> list;

    public CustomDequeue() {
        list = new LinkedList<>();
    }

    public void push(final T element) {
        List<T> templist = new LinkedList<T>();
        try {
            templist.add(element);
            templist.addAll(list);
            list = templist;
        } catch (Exception e) {
            throw e;
        }
    }

    public T pop() {
        T object;
        try {
            object = list.get(0);
            list.remove(object);
            return object;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean add(final T element) {
        try {
            list.add(element);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public void addFirst(final T element) {
        List<T> templist = new LinkedList<T>();
        try {
            templist.add(element);
            templist.addAll(list);
            list = templist;
        } catch (Exception e) {
            throw e;
        }
    }

    public void addLast(final T element) {
        try {
            list.add(element);
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean offer(final T element) throws ClassCastException, NullPointerException {
        try {
            list.add(element);
            return true;
        } catch (Exception e) {
            if (Objects.isNull(element)) {
                throw new NullPointerException();
            } else {
                throw new ClassCastException();
            }
        }
    }

    public boolean offerFirst(final T element) throws ClassCastException, NullPointerException {
        List<T> templist = new LinkedList();
        try {
            templist.add(element);
            templist.addAll(list);
            list = templist;
            return true;
        } catch (Exception e) {
            if (Objects.isNull(element)) {
                throw new NullPointerException();
            } else {
                throw new ClassCastException();
            }
        }
    }

    public boolean offerLast(final T element) throws ClassCastException, NullPointerException {
        try {
            list.add(element);
            return true;
        } catch (Exception e) {
            if (Objects.isNull(element)) {
                throw new NullPointerException();
            } else {
                throw new ClassCastException();
            }
        }
    }

    public boolean remove(final Object o) throws NoSuchElementException {
        try {
            list.remove(o);
            return true;
        } catch (Exception e) {
            throw new NoSuchElementException();
        }
    }

    public T remove() throws NoSuchElementException {
        T object;
        try {
            object = list.get(list.size() - 1);
            list.remove(object);
            return object;
        } catch (Exception e) {
            throw new NoSuchElementException();
        }
    }

    public T removeFirst() throws NoSuchElementException {
        T object;
        try {
            object = list.get(0);
            list.remove(object);
            return object;
        } catch (Exception e) {
            throw new NoSuchElementException();
        }
    }

    public T removeLast() throws NoSuchElementException {
        T object;
        try {
            object = list.get(list.size() - 1);
            list.remove(object);
            return object;
        } catch (Exception e) {
            throw new NoSuchElementException();
        }
    }

    public boolean removeFirstOccurrence(final Object obj) throws NoSuchElementException {
        try {
            for (T element : list) {
                if (element.equals(obj)) {
                    list.remove(obj);
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public boolean removeLastOccurrence(final Object obj) throws NoSuchElementException {
        Collections.reverse(list);
        try {
            for (T element : list) {
                if (element.equals(obj)) {
                    list.remove(obj);
                    Collections.reverse(list);
                    return true;
                }
            }
        } catch (Exception e) {
            Collections.reverse(list);
            return false;
        }
        Collections.reverse(list);
        return false;
    }

    public T poll() {
        T object;
        try {
            object = list.get(list.size() - 1);
            list.remove(object);
            return object;
        } catch (Exception e) {
            return null;
        }
    }

    public T pollFirst() {
        T object;
        try {
            object = list.get(0);
            list.remove(object);
            return object;
        } catch (Exception e) {
            return null;
        }
    }

    public T pollLast() {
        T object;
        try {
            object = list.get(list.size() - 1);
            list.remove(object);
            return object;
        } catch (Exception e) {
            return null;
        }
    }

    public T element() {
        return list.get(list.size() - 1);
    }

    public T getFirst() {
        return list.get(0);
    }

    public T getLast() {
        return list.get(list.size() - 1);
    }

    public T peek() {
        try {
            return list.get(list.size() - 1);
        } catch (Exception e) {
            return null;
        }
    }

    public T peekFirst() {
        try {
            return list.get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public T peekLast() {
        try {
            return list.get(list.size() - 1);
        } catch (Exception e) {
            return null;
        }
    }

    public void clear() {
        list = new LinkedList<T>();
    }

    public boolean contains(Object o) {
        for (T el : list) {
            if (el.equals(o)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsAll(final Collection<?> collection) {
        for (Object element : collection) {
            if (!list.contains((T) element)) {
                return false;
            }
        }
        return true;
    }

    public boolean addAll(final Collection<? extends T> collection) {
        try {
            for (Object element : collection) {
                list.add((T) element);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean removeAll(final Collection<?> collection) {
        try {
            for (Object element : collection) {
                list.remove((T) element);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean retainAll(final Collection<?> collection) {
        try {
            for (T element : list) {
                if (!collection.contains(element)) {
                    list.remove(element);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Object[] toArray() {
        return list.toArray();
    }

    public <T> T[] toArray(final T[] a) throws NullPointerException {
        try {
            return list.toArray(a);
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return list.size() == 0;
    }

    public Iterator descendingIterator() {
        return list.iterator();
    }

    public Iterator iterator() {
        return list.iterator();
    }
}
