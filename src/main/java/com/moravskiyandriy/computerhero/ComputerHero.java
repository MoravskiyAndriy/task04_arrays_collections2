package com.moravskiyandriy.computerhero;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.*;

public class ComputerHero {
    int[] doors;
    int[] doorsAdventures;
    int strength;
    private boolean isAlive = true;
    private List<int[]> routes = new ArrayList<>();
    private static final Logger logger = LogManager.getLogger(ComputerHero.class);

    public ComputerHero(final int[] array) {
        Properties prop = new Properties();
        doors = array;
        doorsAdventures = new int[array.length];
        strength = Optional.ofNullable(prop.
                getProperty("HERO_STRENGTH")).
                map(Integer::valueOf).
                orElse(Constants.HERO_STRENGTH);
        generateAdventures();
    }

    private boolean isAlive() {
        return isAlive;
    }

    public void setAlive(final boolean alive) {
        isAlive = alive;
    }

    private String formStartString(final int[] array) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] == 1) {
                res.append("+" + doorsAdventures[i] + "  ");
            } else {
                res.append("-" + doorsAdventures[i] + "  ");
            }
        }
        return new String(res);
    }

    private String formString(final int[] array) {
        StringBuilder res = new StringBuilder();
        for (int val : array) {
            res.append(val + " ");
        }
        return new String(res);
    }

    private void generateAdventures() {
        Properties prop = new Properties();
        try (InputStream input = ComputerHero.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (Exception ex) {
            logger.warn("Problems with config filew");
        }

        int maxArtifactStrength = Optional.ofNullable(prop.getProperty("MAX_ARTIFACT_STRENGTH")).
                map(Integer::valueOf).orElse(Constants.MAX_ARTIFACT_STRENGTH);
        int minArtifactStrength = Optional.ofNullable(prop.getProperty("MIN_ARTIFACT_STRENGTH")).
                map(Integer::valueOf).orElse(Constants.MIN_ARTIFACT_STRENGTH);
        int maxMonsterStrength = Optional.ofNullable(prop.getProperty("MAX_MONSTER_STRENGTH")).
                map(Integer::valueOf).orElse(Constants.MAX_MONSTER_STRENGTH);
        int minMonsterStrength = Optional.ofNullable(prop.getProperty("MIN_MONSTER_STRENGTH")).
                map(Integer::valueOf).orElse(Constants.MIN_MONSTER_STRENGTH);
        Random rand = new Random();
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] == 1) {
                doorsAdventures[i] = (int) (rand.nextDouble() * (maxArtifactStrength -
                        minArtifactStrength) + minArtifactStrength);
            } else {
                doorsAdventures[i] = (int) (rand.nextDouble() * (maxMonsterStrength -
                        minMonsterStrength) + minMonsterStrength);
            }
        }
        logger.info("Behind doors: " + formStartString(doorsAdventures));
    }

    private void genRoute(final int doorNumber, final int[] checkedDoors) {
        int[] nextDoors = new int[checkedDoors.length + 1];
        for (int i = 0; i < checkedDoors.length; i++) {
            nextDoors[i] = checkedDoors[i];
        }
        nextDoors[checkedDoors.length] = doorNumber;
        boolean wasThere;
        for (int j = 0; j < doors.length; j++) {
            wasThere = false;
            for (int k = 0; k < nextDoors.length; k++) {
                if (j == nextDoors[k]) {
                    wasThere = true;
                }
            }
            if (!wasThere) {
                genRoute(j, nextDoors);
            }
        }
        if (nextDoors.length == doors.length) {
            routes.add(nextDoors);
            return;
        }
    }

    private void doorAction(final int doorNumber) {
        if (doors[doorNumber] == 1) {
            strength += doorsAdventures[doorNumber];
        } else {
            if (!(strength >= doorsAdventures[doorNumber])) {
                isAlive = false;
            }
        }
    }

    public void generateRoutes() {
        for (int i = 0; i < doors.length; i++) {
            int[] mas = new int[0];
            genRoute(i, mas);
        }
    }

    public void checkGoodFate() {
        Properties prop = new Properties();
        generateRoutes();
        List<int[]> goodRoutes = new ArrayList<>();
        Set<List<Integer>> badRoutes = new HashSet<>();
        boolean goodFlag;
        for (int[] route : routes) {
            List<Integer> badRoute = new ArrayList<>();
            goodFlag = true;
            strength = Optional.ofNullable(prop.getProperty("HERO_STRENGTH")).
                    map(Integer::valueOf).orElse(Constants.HERO_STRENGTH);
            isAlive = true;
            for (int i = 0; i < route.length; i++) {
                badRoute.add(route[i]);
                doorAction(route[i]);
                if (!isAlive()) {
                    goodFlag = false;
                    badRoutes.add(badRoute);
                    break;
                }
            }
            if (goodFlag) {
                goodRoutes.add(route);
            }
        }
        logger.info("\nTotal deaths  " + badRoutes.size());
        logger.info("\nGood routes:  " + goodRoutes.size());
        if (goodRoutes.size() != 0) {
            logger.info("\nGood route example: " + formString(goodRoutes.get(0)));
        }
    }
}
