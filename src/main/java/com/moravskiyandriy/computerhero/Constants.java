package com.moravskiyandriy.computerhero;

public class Constants {
    private Constants() {
    }

    public static final int BIG_NUMBER = 10000;
    static final int MIN_ARTIFACT_STRENGTH = 10;
    static final int MAX_ARTIFACT_STRENGTH = 80;
    static final int MIN_MONSTER_STRENGTH = 5;
    static final int MAX_MONSTER_STRENGTH = 100;
    static final int HERO_STRENGTH = 25;
    public static final int MIN_ARRAY_SIZE=100;
    public static final String SENT = "Afghanistan, Albania, Algeria, American Samoa, Andorra, Angola, " +
            "Ecuador, Egypt, El Salvador, Equatorial Guinea, Eritrea, Estonia, Ethiopia, " +
            "Faroe Islands, Fiji, Finland, France, France, Metropolitan, French Guiana, French Polynesia, " +
            "Belarus, Belgium, Belize, Benin, Bermuda, Bhutan, Bolivia, Bosnia and Herzegovina, " +
            "Bulgaria, Burkina Faso, Burundi, Cambodia, Cameroon, Canada, Cape Verde, Cayman Islands, " +
            "Central African Republic, Chad, Chile, China, Christmas Island, Cocos (Keeling Islands), " +
            "Colombia, Comoros, Congo, Cook Islands, Costa Rica, Cote D'Ivoire (Ivory Coast), Croatia ," +
            "Australia, Austria, Azerbaijan, Bahamas, Bahrain, Bangladesh, Barbados, " +
            " Cuba, Cyprus, Czech Republic, Denmark, Djibouti, Dominica, Dominican Republic, East Timor, " +
            "French Southern Territories, Gabon, Gambia, Georgia, Germany, Ghana, Gibraltar, Greece, Greenland";
}
