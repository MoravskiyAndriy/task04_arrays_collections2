package com.moravskiyandriy.twostringclass;

public class TwoStringClass {
    private String a;
    private String b;
    ;

    public TwoStringClass(final String a, final String b) {
        this.a = a;
        this.b = b;
    }

    public String getA() {
        return a;
    }

    public void setA(final String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(final String b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return a + " " + b + ", ";
    }

}
