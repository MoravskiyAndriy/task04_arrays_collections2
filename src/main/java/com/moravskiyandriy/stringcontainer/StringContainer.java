package com.moravskiyandriy.stringcontainer;

import com.moravskiyandriy.computerhero.Constants;

import java.util.Optional;
import java.util.Properties;

public class StringContainer {
    String[] array;
    private int iterator = 0;
    private static int minArraySize;

    public StringContainer() {
        Properties prop = new Properties();
        minArraySize = Optional.ofNullable(prop.getProperty("MIN_ARRAY_SIZE")).
                map(Integer::valueOf).orElse(Constants.MIN_ARRAY_SIZE);
        array = new String[minArraySize];
    }

    public void add(final String s) {
        if (iterator > array.length - 1) {
            String[] newArray = new String[array.length + minArraySize];
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
        }
        array[iterator] = s;
        iterator++;
    }

    public String getStr(final int index) {
        return array[index];
    }
}
