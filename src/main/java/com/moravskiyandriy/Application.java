package com.moravskiyandriy;

import com.moravskiyandriy.arraystasks.ArraysTask;

import com.moravskiyandriy.computerhero.ComputerHero;
import com.moravskiyandriy.computerhero.Constants;
import com.moravskiyandriy.dequeue.CustomDequeue;
import com.moravskiyandriy.stringcontainer.StringContainer;
import com.moravskiyandriy.twostringclass.TwoStringClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.*;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);
    private static final String ADDING_CONSTANT = "Comparatively long line.";

    private static String formString(final Object[] array) {
        StringBuilder res = new StringBuilder();
        for (Object o : array) {
            res.append(o.toString() + " ");
        }
        return new String(res);
    }

    private static String formString(final Collection<?> array) {
        StringBuilder res = new StringBuilder();
        for (Object o : array) {
            res.append(o + " ");
        }
        return new String(res);
    }

    private static void createThirdarrays() {
        ArraysTask<Integer> task = new ArraysTask<Integer>();
        Integer[] arr1 = {1, 2, 3, 4, 5, 6};
        Integer[] arr2 = {5, 6, 7, 8, 9, 0};
        Object[] arr3 = task.createThirdArray(arr1, arr2, "a");
        Object[] arr4 = task.createThirdArray(arr1, arr2, "b");
        logger.info("\ncreateThirdArray({1, 2, 3, 4, 5, 6}," +
                "{5, 6, 7, 8, 9, 0},'a'):\n" + formString(arr3));
        logger.info("\ncreateThirdArray({1, 2, 3, 4, 5, 6}," +
                "{5, 6, 7, 8, 9, 0},'b'):\n" + formString(arr4));
    }

    private static void deleteObjectsFromArray() {
        Integer[] deleteNumbArray = {5, 6, 7, 8, 9, 0, 5, 5, 7, 7};
        deleteNumbArray = new ArraysTask<Integer>(deleteNumbArray).deleteNumbers();
        logger.info("\n{5, 6, 7, 8, 9, 0, 5, 5, 7, 7}." +
                "deleteNumbers():" + formString(deleteNumbArray));
    }

    private static void deleteSeriesFromArray() {
        Integer[] deleteSeriesArray = {5, 5, 7, 8, 9, 9, 9, 9, 9, 0, 5, 0, 0, 0, 5, 7, 7};
        deleteSeriesArray = new ArraysTask<Integer>(deleteSeriesArray).deleteSeries();
        logger.info("\n{5, 5, 7, 8, 9, 9, 9, 9, 9, 0, 5, 0, 0, 0, 5, 7, 7}" +
                ".deleteSeries():" + formString(deleteSeriesArray));
    }

    private static void testCustomStringContainer() {
        Properties prop = new Properties();
        int iterations=Optional.ofNullable(prop.
                getProperty("BIG_NUMBER")).
                map(Integer::valueOf).
                orElse(Constants.BIG_NUMBER);
        logger.info("\nStringContainer: ");
        StringContainer strContainer = new StringContainer();
        logger.info("Start time: " + System.currentTimeMillis());
        for (int i = 0; i < iterations; i++) {
            strContainer.add(ADDING_CONSTANT);
        }
        logger.info("Middle time: " + System.currentTimeMillis());
        List<String> list = new ArrayList<>();
        for (int i = 0; i < iterations; i++) {
            list.add(ADDING_CONSTANT);
        }
        logger.info("End time: " + System.currentTimeMillis());
        logger.info("Speed of work of my custom container, " +
                "based on array, depends heavily on the size of " +
                "incrementation: the more, the faster.");
    }

    private static void testTwoStringClass() {
        Properties prop = new Properties();
        try (InputStream input = ComputerHero.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (Exception ex) {
            logger.warn("Problems with config filew");
        }
        String sent = Optional.ofNullable(prop.
                getProperty("SENT")).
                map(np -> np.toString()).
                orElse(Constants.SENT);
        logger.info("\ntestTwoStringClass: ");
        List<TwoStringClass> list = new ArrayList<>();
        String[] words = sent.split(",");
        TwoStringClass[] array = new TwoStringClass[words.length / 2 / 2 + 1];
        int iterator = 0;
        for (int i = 0; i < words.length / 2; i = i + 2) {
            list.add(new TwoStringClass(words[i], words[i + 1]));
            array[iterator] = new TwoStringClass(words[i], words[i + 1]);
            iterator++;
        }
        logger.info("\nComparator.comparing(TwoStringClass::getA");
        Collections.sort(list, Comparator.comparing(TwoStringClass::getA));
        Arrays.sort(array, Comparator.comparing(TwoStringClass::getA));
        logger.info("\n" + formString(list));
        logger.info("\n" + formString(array));
        logger.info("\nComparator.comparing(TwoStringClass::getB");
        Collections.sort(list, Comparator.comparing(TwoStringClass::getB));
        Arrays.sort(array, Comparator.comparing(TwoStringClass::getB));
        logger.info("\n" + formString(list));
        logger.info("\n" + formString(array));
    }

    private static void testCustomDequeue() {
        logger.info("\ntestCustomDequeue");
        CustomDequeue<TwoStringClass> cDQueue = new CustomDequeue<>();
        cDQueue.addFirst(new TwoStringClass("a", "a"));
        cDQueue.addFirst(new TwoStringClass("a", "b"));
        cDQueue.addLast(new TwoStringClass("a", "c"));
        cDQueue.addLast(new TwoStringClass("a", "d"));
        cDQueue.addFirst(new TwoStringClass("a", "e"));
        int dequeueSize = cDQueue.size();
        for (int i = 0; i < dequeueSize; i++) {
            logger.info(cDQueue.pollFirst());
        }
    }

    private static void game() {
        logger.info("\nGame starts: ");
        int[] doors = {1, 1, 0, 1, 0};
        ComputerHero hero = new ComputerHero(doors);
        hero.checkGoodFate();
        logger.info("\nGame ends: ");
    }

    public static void main(String[] args) {
        logger.info("START\n");
        createThirdarrays();
        deleteObjectsFromArray();
        deleteSeriesFromArray();
        testCustomStringContainer();
        testTwoStringClass();
        testCustomDequeue();
        game();
        logger.info("\nEND");
    }
}
