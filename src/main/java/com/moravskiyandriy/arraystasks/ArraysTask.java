package com.moravskiyandriy.arraystasks;

import org.apache.logging.log4j.core.util.ArrayUtils;

import java.util.*;

public class ArraysTask<T> {
    T[] array;

    public ArraysTask() {
    }

    public ArraysTask(T[] array) {
        this.array = array;
    }

    public T[] createThirdArray(T[] array1, T[] array2, String option) {
        Set<T> set = new HashSet<T>();
        if (option.equals("a")) {
            set = new HashSet<T>();
            for (T element1 : array1) {
                for (T element2 : array2) {
                    if (element1.equals(element2)) {
                        set.add(element1);
                    }
                }
            }
            return (T[]) set.toArray();
        }
        if (option.equals("b")) {
            set = new HashSet<T>();
            for (T element : array1) {
                set.add(element);
            }
            for (Object element1 : array1) {
                for (T element2 : array2) {
                    if (element1.equals(element2)) {
                        set.remove(element1);
                    }
                }
            }
            return (T[]) set.toArray();
        }
        return (T[]) set.toArray();
    }

    public T[] deleteNumbers() {
        Set<T> set = new HashSet<>();
        List<Integer> removeList = new ArrayList<>();
        int counter;
        for (T element1 : array) {
            counter = 0;
            for (T element2 : array) {
                if (element1.equals(element2)) {
                    counter++;
                }
            }
            if (counter > 2) {
                set.add(element1);
            }
        }
        for (T element : set) {
            for (int i = 0; i < array.length; i++) {
                if (array[i].equals(element)) {
                    removeList.add(i);
                }
            }
        }
        Collections.sort(removeList);
        Collections.reverse(removeList);
        for (int index : removeList) {
            array = ArrayUtils.remove(array, index);
        }
        return array;
    }

    public T[] deleteSeries() {
        List<Integer> removeList = new ArrayList<>();
        for (int i = 1; i < array.length; i++) {
            if (array[i].equals(array[i - 1])) {
                removeList.add(i);
            }
        }
        Collections.sort(removeList);
        Collections.reverse(removeList);
        for (int index : removeList) {
            array = ArrayUtils.remove(array, index);
        }
        return array;
    }
}
